call plug#begin()
Plug 'lervag/vimtex'
Plug 'majutsushi/tagbar'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'leafgarland/typescript-vim'
Plug 'scrooloose/syntastic'
Plug 'Valloric/YouCompleteMe'
Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'Quramy/tsuquyomi', { 'for': 'typescript' }
"Plug 'ap/vim-css-color', { 'for': ['html', 'css', 'less'] }
Plug 'tpope/vim-sleuth'
call plug#end()

" Use UTF-8 (for YCM)
set encoding=utf-8

" VimTeX
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique @pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'
let g:vimtex_index_show_help = 0
let g:vimtex_latexmk_continuous = 0
let g:vimtex_latexmk_background = 1

" tagbar
filetype on
nnoremap <silent> <F9> :TagbarToggle<CR>
let g:tagbar_autoclose = 1
let g:tagbar_sort = 0
let g:airline#extensions#tagbar#flags = 'f'
let g:airline#extensions#whitespace#mixed_indent_algo = 2
let g:airline#extensions#whitespace#checks = [ 'indent', 'long', 'mixed-indent-file' ]

" airline
set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='bubblegum'

" syntastic, tsuquyomi
let g:tsuquyomi_disable_quickfix = 1
let g:tsuquyomi_completion_detail = 1
let g:syntastic_typescript_checkers = ['tsuquyomi']

" Use soft line wrapping
set wrap linebreak nolist
set showbreak=->
set breakindent
set tabstop=4
set shiftwidth=4
set expandtab
set list
set listchars=tab:»\ ,trail:·

" Use relative line numbering, except in insert mode
set number
set relativenumber
autocmd InsertEnter * :set number
autocmd InsertLeave * :set relativenumber

set timeoutlen=1000 ttimeoutlen=0

" Keep some lines visible around the cursor
set scrolloff=5

" Make ~ behave as an operator
set tildeop

" Remove the vertical bars from vertical-split borders
set fillchars+=vert:\ 

" Swap ; and :
:nnoremap : ;
:nnoremap ; :

" Swap q and @
:nnoremap q @
:nnoremap @ q

" Let "hj" exit insert mode instead of <esc>
:inoremap hj <esc>

" Show command-line completions
:set wildmenu

