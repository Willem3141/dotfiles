Setting up a new machine:

* make directory `~/Git`, clone this repo into it
* install vim, fish, powerline
* configure vim:
   * `ln -s Git/dotfiles/vimrc .vimrc`
   * `curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`
   * open vim, run `:PlugInstall`
* configure fish:
   * `chsh willem -s /usr/bin/fish`
   * edit `.config/fish/config.fish`:
```fish
set fish_function_path $fish_function_path "/usr/share/powerline/bindings/fish"
source /usr/share/powerline/bindings/fish/powerline-setup.fish
powerline-setup
```
* `touch $HOME/.hushlogin`
* enable authentication without password:
   * copy my own public key from my system: `xclip -sel clip < ~/.ssh/id_rsa.pub`
   * paste that into `.ssh/authorized_keys`
   * in `/etc/ssh/sshd_config`, set `PasswordAuthentication no` to disable password logins
